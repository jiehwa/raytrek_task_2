# Find and Replace



## Installation
` pip install -r requirements.txt `


## Project Structure
```
raytesk_task_2
|- find_and_replace.py
|- input.tsf   [raw data]
|- param2.tsf   [argument data]
|- input_replace.tsf  [replaced data]
```
#### Argument Data format
For example, param2.tsf:

```
[
["Vt_id", "Id", "Idrain"],  <--- first replacement
["Fvsi", "Vf", "Vforce"]    <--- second replacement

( ... vice versa...)
]
```

### Replaced Data
Replaced Data name as suffiex "_replace" on raw data

Ex , raw data : input15.tsf, replaced data : input15_replace.tsf


## Example

` python3 --file <raw data>  --argument <argument data> '
![run command](imgs/command.png)



### Testing for regex literal string

as expression:
![pdf image] (imgs/description.png)

raw data that have "Vt_id2" 
![input15.tsf] (imgs/input15.png)



replaced data : input of Algorithm "Vt_id2" remains "Id"
![input15_replace] (imgs/input15_replace.png)


