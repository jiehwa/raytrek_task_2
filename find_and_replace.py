import pandas as pd
import numpy as np
import ast
import argparse
import time


def _find_n_replace(lst, df):
    """
    lst = [idx, pat, repl]
    idx : string of "Algorithm" / "Algorithm" 的參數
    pat : string of "Input" to be replace / “input” 要修改的參數
    repl : replacement string of pat / “input” 要改成的參數
    """

    idx, pat, repl = lst
    filter = (df["Algorithm"] == idx)
    modify_col = (df[filter]["Input"]).str.replace(pat, repl,regex = False) 
    df.update(modify_col)
    return df


def main(raw_file, arg_file):
    ### 1. 讀取原始資料
    df = pd.read_csv(raw_file, sep='\t', header = 1) # header = 1 , means that take the second row as title in dataframe

    ### 2. 讀取即將要修改的參數
    with open(arg_file) as f : 
        input_data = f.read()
    lst = ast.literal_eval(input_data)

    for i in lst:
        df = _find_n_replace(i, df)
    
    ### 3. 儲存
    repl_file = raw_file.split(".",1)[0] + "_replace.tsf"
    df.to_csv(repl_file,sep='\t')

if __name__ == "__main__":
    starting = time.time()
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--file", type=str, required=True, help="raw file")
    ap.add_argument("-a", "--argument", type=str, required=True, help="argument file")
    args = vars(ap.parse_args())

    main(args["file"], args["argument"])
    
    print ("Replacement Done!   Time consumed : {} s".format(str(time.time()-starting)) )
    





